/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
    state = {
        placeName: '',
        places: []
    };

    placeNameChangeHandler = val => {
        this.setState({
            placeName: val
        })
    };

    placeSubmitHandler = () => {
        if (this.state.placeName.trim() === "") {
            return;
        }
        this.setState(prevState => {
            return {
                places: prevState.places.concat(prevState.placeName)
            };
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.justCheck}>
                    <TextInput
                        style={{width: 300, borderWidth: 1, borderColor: 'black'}}
                        placeholder="Awesome Place"
                        value={this.state.placeName}
                        onChangeText={this.placeNameChangeHandler}/>
                    <TouchableOpacity style={{backgroundColor: 'red'}} onPress={this.placeSubmitHandler}>
                        <Text>ADD</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    justCheck: {
        flexDirection: 'row',
        height: 50,
        width:'100%',
        justifyContent: 'space-evenly'
    },
});
